<?php
session_start();

?>
<!DOCTYPE html>
<html lang="ru">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>
    Создание сайтов в Пскове и Санкт-Петербурге. Реклама и продвижение сайтов
    Псков.
  </title>
  <link rel="stylesheet" href="./style.css" />
</head>

<body link="#dcd9dc" vlink="#dcd9dc" alink="#dcd9dc">
  <style>
    a {
      text-decoration: none;
    }
  </style>

  <div>
    <div>
      <header>
        <div id="about1">
          <div class="ab">
            <a href="#s1" class="nav">О компании</a>
            <a href="#" class="nav">Команда</a>
            <a href="#s3" class="nav">Клиенты</a>
            <a href="#s2" class="nav">Наши приемущества</a>
            <a href="#" class="nav">Вакансии</a>
            <a href="#" class="nav">Контакты</a>
            <a href="#" class="nav">Псков +7 (8112) 70 20 80</a>
            <a href="#" class="nav">Санкт-Петербург +7 (812) 982-19-12</a>
            <?php
            if (!$_SESSION['user']) {
            ?>
              <a href="signin.php" action="signin.php" class="" role="button">Войти</a>
              <a href="reg.php" action="reg.php" class="" role="button">Зарегестрироваться
              </a>
            <?php
            }
            ?>

            <?php
            if ($_SESSION['user']) {
            ?>
              <a href="" action="" class="" role="button"><?= $_SESSION['user']['login'] ?></a>
              <a href="logout.php" action="logout.php" class="" role="button">Выйти
              </a>
            <?php
            }
            ?>




          </div>
        </div>
        <div id="about">
          <div id="about2">
            <div class="ab2">
              <img src="https://endesign.ru/wp-content/uploads/2020/10/endesign.png" />
              <a href="#" class="navi">Главная</a>
              <a href="#" class="navi">Разработка сайтов</a>
              <a href="#" class="navi">Мобильные приложения</a>
              <a href="#" class="navi">Web разработка</a>
              <a href="#" class="navi">Портфолио</a>
            </div>
          </div>
          <div id="about3">
            <p>
              <a class="nav2">Развиваем цифровые продукты и системы продвижения.</a>
              <br />
              <a class="nav2">WEB студия</a>
            </p>

            <div class="bt">
              <a href="portfolio" class="es" role="button">Перейти в портфолио</a>
            </div>
          </div>
        </div>
      </header>

      <div class="ez" id="s1">
        <h1 class="hhh">Веб студия ENDESIGN</h1>
      </div>
      <div>
        <p class="hhh">
          Наша задача не просто создавать сайты. Мы создаем эффективные
          бизнес-представительства компаний с уникальным дизайном в сети
          интернет, которые <br />помогают развивать бизнес, увеличивать
          продажи, повышать узнаваемость продукции
        </p>
      </div>
      <div>
        <h1 class="hhh">Наши услуги</h1>
      </div>
    </div>
    <div class="znachki">
      <div class="znachki2">
        <img src="https://endesign.ru/wp-content/uploads/2020/10/domain-registration-1.png" />
        <a href="#" class="nav">СОЗДАНИЕ САЙТОВ</a>
        <img src="https://endesign.ru/wp-content/uploads/2020/10/mobile-phone.png" />
        <a href="#" class="nav">МОБИЛЬНЫЕ ПРИЛОЖЕНИЯ</a>
        <img src="https://endesign.ru/wp-content/uploads/2020/10/computer.png" />
        <a href="#" class="nav">ПРОДВИЖЕНИЕ</a>
        <img src="https://endesign.ru/wp-content/uploads/2020/10/coding.png" />
        <a href="#" class="nav">WEB РАЗРАБОТКА</a>
      </div>
    </div>

    <div class="menu">
      <div class="phot">
        <img src="https://endesign.ru/wp-content/uploads/2020/10/picture.jpg" alt="" />
      </div>
      <div class="texx">
        <p class="nenav">
          Наша компания занимается созданием сайтов в Пскове.
        </p>

        <p class="nenav">
          На текущий момент сайт стал одной из ключевых вещей, которые
          необходимы каждой компании.
        </p>

        <p class="nenav">
          Задачи, которые компании отводят своему сайту, различные:
          информационная визитка, канал продаж, промо-акция. При этом диапазон
          стоимости сайтов на рынке весьма значительный. Мы считаем, что сайты
          не должны необоснованно дорого стоить, ведь сайт – это программный
          продукт, который имеет определенные трудозатраты на создание и
          настройку.
        </p>

        <p class="nenav">
          Своим клиентам мы предлагаем ряд основных продуктов по созданию
          сайтов в Пскове.
        </p>
        <div>
          <a href="portfolio" class="es2" role="button">Подробнее</a>
        </div>
      </div>
    </div>

    <div class="bas">
      <div id="s2">
        <h1 class="hhh">Наши преимущества</h1>
      </div>
      <div class="lin">
        <a class="nenav3">
          <h2 class="nenav2">Широкий спектр услуг</h2>
          <div>
            <span>
              <hr color="#e06618" size="3px" class="solid" />
            </span>
          </div>
          <p class="nenav2">
            Наша компания может провести Ваш проект от разработки сайта до его
            продвижения
          </p>
        </a>

        <a class="nenav3">
          <h2 class="nenav2">Гибкая ценовая политика</h2>
          <div class="polosa">
            <span class="tsvet">
              <hr color="#e06618" size="3px" class="solid" />
            </span>
          </div>
          <p class="nenav2">
            Для полного расчета стоимости услуг, Вам необходимо подробно
            заполнить заявку.
          </p>
        </a>
        <a href="#" class="nenav3">
          <h2 class="nenav2">Большая клиентская аудитория</h2>
          <div>
            <span>
              <hr color="#e06618" size="3px" class="solid" />
            </span>
          </div>
          <p class="nenav2">
            Спектр наших клиентов многообразен. Мы работаем также с
            федеральными регионами.
          </p>
        </a>
      </div>
      <div class="lin">
        <a class="nenav3">
          <h2 class="nenav2">Многолетний опыт</h2>
          <div>
            <span>
              <hr color="#e06618" size="3px" class="solid" />
            </span>
          </div>
          <p class="nenav2">
            Компания enDESIGN уже много лет специализируется на создании
            сайтов, продвижении сайтов.
          </p>
        </a>

        <a class="nenav3">
          <h2 class="nenav2">Квалифицированный коллектив</h2>
          <div>
            <span>
              <hr color="#e06618" size="3px" class="solid" />
            </span>
          </div>
          <p class="nenav2">
            Мы предлагаем разработку сайтов на разных платформах: Bitrix,
            Joomla, WordPress, Drupal и другие.
          </p>
        </a>

        <a class="nenav3">
          <h2 class="nenav2">Разные платформы</h2>
          <div>
            <span>
              <hr color="#e06618" size="3px" class="solid" />
            </span>
          </div>
          <p class="nenav2">
            Мы предлагаем разработку сайтов на разных платформах: Bitrix,
            Joomla, WordPress, Drupal и другие.
          </p>
        </a>
      </div>
    </div>
  </div>
  <div style="margin: 0 auto; max-width: 600px">
    <ul class="bxslider" id="s3">
      <li>
        <img src="https://endesign.ru/wp-content/uploads/2020/10/picture.jpg" />
      </li>
      <li>
        <img src="https://endesign.ru/wp-content/uploads/2020/10/picture.jpg" />
      </li>
      <li>
        <img src="https://endesign.ru/wp-content/uploads/2020/10/picture.jpg" />
      </li>
      <li>
        <img src="https://endesign.ru/wp-content/uploads/2020/10/picture.jpg" />
      </li>
    </ul>
  </div>

  <div>
    <h1 class="hhh">Примеры недавних работ</h1>
    <p class="zdes">
      ЗДЕСЬ ВЫ МОЖЕТЕ ОЗНАКОМИТЬСЯ С НАШИМИ РЕАЛИЗОВАННЫМИ РАБОТАМИ
    </p>
    <div class="cia">
      <div class="container">
        <a href=""><img class="kart" src="https://endesign.ru/wp-content/uploads/2020/10/qwe.png" /></a>
        <div class="hh">
          <p class="hh">ШИРОКОФОРМАТНАЯ ПЕЧАТЬ</p>
        </div>
      </div>
      <div class="container">
        <img class="kart" src="https://endesign.ru/wp-content/uploads/2020/10/zastavka.jpg" />
        <div class="hh">
          <p class="hh">ЮТТА STARS</p>
        </div>
      </div>
      <div class="container">
        <img class="kart" src="	https://endesign.ru/wp-content/uploads/2020/10/logotip-plesca.png" />
        <div class="hh">
          <p class="hh">PLESCA</p>
        </div>
      </div>
    </div>
  </div>

  <div class="aforma">
    <div class="slova">
      <p class="vop">Остались вопросы?</p>
      <br />
      <p class="vop2">
        ОПИШИТЕ ТРЕБОВАНИЯ К САЙТУ И ПОЛУЧИТЕ БЕСПЛАТНУЮ КОНСУЛЬТАЦИЮ
      </p>
    </div>

    <div class="forma">
      <form action="check2.php" method="post" target="_blank">
        <div class="sle">
          <div>
            <p class="tele">ИМЯ</p>
            <input type="text" name="username" placeholder="введите имя" class="name" required />
          </div>

          <div class="em">
            <p class="tele">EMAIL</p>
            <input type="email" name="email" placeholder="введите email" class="name" required />
            <br />
          </div>
        </div>

        <div>
          <p class="tele">ТЕЛЕФОН</p>
          <input type="tel" name="tel" placeholder="введите номер телефона" class="name" />
          <br />
        </div>
        <div class="mf">
          <p class="tele2">КАКАЯ УСЛУГА ВАС ИНТЕРЕСУЕТ</p>
          <select class="vibor" name="vib">
            <option label="Разработка сайтов"></option>
            <option label="мобильное приложение"></option>
            <option label="WEB разработка"></option>
          </select>
        </div>
        <input type="checkbox" name="checkbox" class="" />
        <p color="#dcd9dc">Даю согласие на обработку персональных данных</p>
        <br />
        <p class="tele">КОММЕНТАРИЙ</p>
        <textarea name="message" placeholder="ваше сообщение" class="kom" required></textarea>
        <br />
        <button type="submit" class="es3">отправить</button>
      </form>
    </div>
  </div>

  <footer>
    <div class="foot">
      <div class="lev">
        <div>
          <img src="https://endesign.ru/wp-content/uploads/2020/10/endesign.png" />
        </div>
        <p class="fots">
          Псков<a href="#" class="fots">+7 (8112) 70 20 80</a>
        </p>
        <p class="fots">
          Санкт-Петербург<a href="#" class="fots">+7 (812) 982-19-12</a>
        </p>
        <div>
          <img src="https://cdn.discordapp.com/attachments/509410070937862144/884823264982081566/inst.png" />
          <img src="https://cdn.discordapp.com/attachments/509410070937862144/884823267871952966/sap.png" />
          <img src="https://cdn.discordapp.com/attachments/509410070937862144/884823304454676592/vk.png" />
          <img src="https://cdn.discordapp.com/attachments/509410070937862144/884823309873737728/viber.png" />
          <img src="https://cdn.discordapp.com/attachments/509410070937862144/884824192078467182/unknown.png" />
        </div>
      </div>
      <div class="prav">
        <ul class="ul">
          <li class="li">
            <h3 class="h3"><a href="">Главная</a></h3>
            <ul class="inner_ul">
              <li class="inner_li">
                <a href="" color="#dcd9dc">разработка сайтов</a>
              </li>
              <li class="inner_li"><a href=""> мобильные приложения</a></li>
              <li class="inner_li"><a href="">web разработка</a></li>
              <li class="inner_li"><a href="">продвижение</a></li>
            </ul>
          </li>
          <li class="li">
            <h3 class="h3">О нас</h3>
          </li>
          <li class="li">
            <h3 class="h3">Контакты</h3>
          </li>
          <li class="li">
            <h3 class="h3">Портфолио</h3>
          </li>
        </ul>
      </div>
    </div>
  </footer>
  <!-- jQuery library (served from Google) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!-- bxSlider Javascript file -->
  <script src="js/jquery.bxslider.js"></script>
  <script src="js/custom.js"></script>
  <!-- bxSlider CSS file -->
  <link href="css/jquery.bxslider.css" rel="stylesheet" />
</body>

</html>
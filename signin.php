<?php
session_start();
?>




<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>
        Создание сайтов в Пскове и Санкт-Петербурге. Реклама и продвижение сайтов
        Псков.
    </title>
    <link rel="stylesheet" href="./style.css" />
</head>

<body link="#dcd9dc" vlink="#dcd9dc" alink="#dcd9dc">
    <style>
        a {
            text-decoration: none;
        }
    </style>

    <div>
        <header>
            <div id="about1">
                <div class="ab">
                    <a href="#s1" class="nav">О компании</a>
                    <a href="#" class="nav">Команда</a>
                    <a href="#s3" class="nav">Клиенты</a>
                    <a href="#s2" class="nav">Наши приемущества</a>
                    <a href="#" class="nav">Вакансии</a>
                    <a href="#" class="nav">Контакты</a>
                    <a href="#" class="nav">Псков +7 (8112) 70 20 80</a>
                    <a href="#" class="nav">Санкт-Петербург +7 (812) 982-19-12</a>
                </div>
            </div>

        </header>

    </div>


    <div class="place">


        <form action="sign.php" method="POST">
            <input type="text" name="login" placeholder="введите login" class="name" required /><br>
            <input type="password" name="pass" placeholder="введите пароль" class="name" required /> <br>
            <button type="submit">Войти</button>
            <?php
            if ($_SESSION['messa']) {
                echo  '<p class="msg">' . $_SESSION['messa'] . '</p>';
            }
            unset($_SESSION['messa']);
            ?>



        </form>


    </div>


    <footer>
        <div class="foot">
            <div class="lev">
                <div>
                    <img src="https://endesign.ru/wp-content/uploads/2020/10/endesign.png" />
                </div>
                <p class="fots">
                    Псков<a href="#" class="fots">+7 (8112) 70 20 80</a>
                </p>
                <p class="fots">
                    Санкт-Петербург<a href="#" class="fots">+7 (812) 982-19-12</a>
                </p>
                <div>
                    <img src="https://cdn.discordapp.com/attachments/509410070937862144/884823264982081566/inst.png" />
                    <img src="https://cdn.discordapp.com/attachments/509410070937862144/884823267871952966/sap.png" />
                    <img src="https://cdn.discordapp.com/attachments/509410070937862144/884823304454676592/vk.png" />
                    <img src="https://cdn.discordapp.com/attachments/509410070937862144/884823309873737728/viber.png" />
                    <img src="https://cdn.discordapp.com/attachments/509410070937862144/884824192078467182/unknown.png" />
                </div>
            </div>
            <div class="prav">
                <ul class="ul">
                    <li class="li">
                        <h3 class="h3"><a href="">Главная</a></h3>
                        <ul class="inner_ul">
                            <li class="inner_li">
                                <a href="" color="#dcd9dc">разработка сайтов</a>
                            </li>
                            <li class="inner_li"><a href=""> мобильные приложения</a></li>
                            <li class="inner_li"><a href="">web разработка</a></li>
                            <li class="inner_li"><a href="">продвижение</a></li>
                        </ul>
                    </li>
                    <li class="li">
                        <h3 class="h3">О нас</h3>
                    </li>
                    <li class="li">
                        <h3 class="h3">Контакты</h3>
                    </li>
                    <li class="li">
                        <h3 class="h3">Портфолио</h3>
                    </li>
                </ul>
            </div>
        </div>
    </footer>
</body>

</html>